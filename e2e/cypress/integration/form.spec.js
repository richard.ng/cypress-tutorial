import HomePage from "../support/PageObjects/HomePage"

const homePage = new HomePage();

describe("Form test", () => {
  it("Can fill the form", () => {

    homePage.Server()

    homePage.Submit()

    cy.contains("Form saved!");
  });
});
