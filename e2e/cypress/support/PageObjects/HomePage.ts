class HomePage {
  private name: string;
  private email: string;
  private text: string;

  constructor() {
    this.name = Cypress.env("name");
    this.email = Cypress.env("email");
    this.text = Cypress.env("text");
  }

  public Submit() {
    this.visit();
    this.getName(this.name);
    this.getEmail(this.email);
    this.getText(this.text);
    this.submitButton();
  }

  private visit() {
    cy.visit('/')
  }

  private getName(text: string) {
    cy.get('input[name="name"]').type(text).should("have.value", text);
  }

  private getEmail(text: string) {
    cy.get('input[name="email"]').type(text).should("have.value", text);
  }

  private getText(text: string) {
    cy.get("textarea").type(text).should("have.value", text);
  }

  public Server() {
    cy.server();
    cy.route({
      url: "/users/**",
      method: "POST",
      response: { status: "Form saved!", code: 201 }
    });
  }

  private submitButton() {
    cy.get("form").submit();
  }
}

export default HomePage;
